Maurer Law provides legal representation for for personal injury claims, employment discrimination cases, and criminal dui cases. We help our clients navigate the complexities of their unique legal situation. As a trial attorney with years of experience, Joshua Maurer truly cares about his clients.

Address: 1604 W Dean Ave, Spokane, WA 99201

Phone: 509-838-9111